<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lists</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>
<h2>Untitled Docment</h2>
<ol>
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<h2>Ordered List with Letter</h2>
<ol type="A">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<h2>Ordered List with Number</h2>
<ol type="1">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>
<h2>Ordered List with Lowercase letters</h2>
<ol type="a">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<h2>Ordered List with Uppercase Roman Numbers</h2>
<ol type="I">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<h2>Ordered List with lowercase Roman Numbers</h2>
<ol type="i">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<h2>Ordered List with lowercase Roman Numbers</h2>
<ol start="5">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>
<h2>Ordered List with Reversed</h2>
<ol reversed>
    <li>Black Dog - Led Zeppelin</li>
    <li>Cat Scratch Fever - Ted Nugent</li>
    <li>Who Let The Dogs Out? - The Baha Men</li>
    <li>Motley Crue Treat Me Like The Dog That I Am - Motley Crue</li>
    <li>I'll Be Doggone - Marvin Gaye</li>
</ol>

<ol type="a" start="5">
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>
<ol start="1001" reversed>
    <li>Cofee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
</ol>

<ol>
    <li>first item</li>
    <li>second item  <!-- closing </li> tag not here! -->
        <ol>
            <li>second item first subitem</li>
            <li>second item second subitem</li>
            <li>second item third subitem</li>
        </ol>
    </li>            <!-- Here's the closing </li> tag -->
    <li>third item</li>
</ol>

<ul type="circle">
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
    <li>Juice</li>
    <li>Cola</li>
    <li>Lemonade</li>
</ul>

<h1>W3school Bootstrap</h1>
<h2>Basic list group</h2>
<ul class="list-group">
    <li class="list-group-item">Coffee</li>
    <li class="list-group-item">Tea</li>
    <li class="list-group-item">Milk</li>
    <li class="list-group-item">Juice</li>
    <li class="list-group-item">Cola</li>
    <li class="list-group-item">Lemonade</li>
</ul>
<br/>
<br/>
<br/>
<h2>Active list group</h2>
<ul class="list-group">
    <li class="list-group-item active">Coffee</li>
    <li class="list-group-item">Tea</li>
    <li class="list-group-item">Milk</li>
    <li class="list-group-item">Juice</li>
    <li class="list-group-item">Cola</li>
    <li class="list-group-item">Lemonade</li>
</ul>

<br/>
<br/>
<br/>
<h2>Disabled list group</h2>
<ul class="list-group">
    <li class="list-group-item disabled">Coffee</li>
    <li class="list-group-item">Tea</li>
    <li class="list-group-item">Milk</li>
    <li class="list-group-item">Juice</li>
    <li class="list-group-item">Cola</li>
    <li class="list-group-item">Lemonade</li>
</ul>
<br/>
<br/>
<br/>
<h2>Action list group</h2>
<ul class="list-group">
    <li class="list-group-item list-group-item-action ">Coffee</li>
    <li class="list-group-item list-group-item-action">Tea</li>
    <li class="list-group-item list-group-item-action">Milk</li>
    <li class="list-group-item list-group-item-action">Juice</li>
    <li class="list-group-item list-group-item-action">Cola</li>
    <li class="list-group-item list-group-item-action">Lemonade</li>
<br/>
<br/>
<br/>
<h2>Contextual Classes</h2>
<ul class="list-group">
    <li class="list-group-item list-group-item-action list-group-item-success ">Coffee</li>
    <li class="list-group-item list-group-item-action list-group-item-secondary">Tea</li>
    <li class="list-group-item list-group-item-action list-group-item-info">Milk</li>
    <li class="list-group-item list-group-item-action list-group-item-warning"">Juice</li>
    <li class="list-group-item list-group-item-action list-group-item-primary">Cola</li>
    <li class="list-group-item list-group-item-action list-group-item-light">Lemonade</li>
</ul>
    <br/>
    <br/>
    <br/>
    <h2>Link items with Contextual Classes</h2>

    <div>
        <a href="#" class="list-group-item list-group-item-action">Action Item</a>
        <a href="#" class="list-group-item list-group-item-success">Success Item</a>
        <a href="#" class="list-group-item list-group-item-secondary">Secondary Item</a>
        <a href="#" class="list-group-item list-group-item-info">Info Item</a>
        <a href="#" class="list-group-item list-group-item-primary">Primary Item</a>




    </div>
</body>
</html>