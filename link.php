<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Links</title>
    <style>
       //unvisited link
        a:link{
           color: aqua;
       }
        //visited link
        a:visited{
           color: blue;
          }
        //mouse over link
        a:hover{
           color: cadetblue;
          }
        //selected link
        a:active{
           color: chocolate;
          }
        //focus link
        a:focus{
           color: limegreen;
          }

    </style>
</head>
<body>
<h5>1. Open the link in a new window (or tab)</h5>

<a href="http://pondit.com/" target="_blank"><b>This is a link</b></a>
<h5>2. Creating an email link</h5>

<a href="mailto:nowhere@mozilla.org">Send email to nowhere</a>

<h5>3. Creating a phone link</h5>

<a href="tel:+491570156">+49 157 0156</a>

<h5>4. Download a file</h5>

<p><a href="ol-tag.html" download>Link to a resource to download</a></p>
</body>
</html>List