<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Text/Typography</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h1>h1 Bootstrap heading (36px)<small> secondary text</small></h1>
    <h2>h1 Bootstrap heading (30px)</h2>
    <h3>h1 Bootstrap heading (24px)</h3>
    <h4>h1 Bootstrap heading (18px)</h4>
    <h5>h1 Bootstrap heading (14px)</h5>
    <h6>h1 Bootstrap heading (12px)</h6>
    <br/>




    <p>Use the mark element to <mark>Highlight</mark>text</p>
    <br/>




    <p>The <abbr title="World Health Organization">WHO</abbr> was founded in 1945./p>
    <br/>




    <p> The blockquote element is used to present content from another source:</p>
    <blockquote>
        <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.</p>
        <footer>From WWF's Website</footer>
    </blockquote>
    <br/>




 <p> The blockquote element is used to present content from another source:</p>
    <blockquote class="blockquote-reverse">
        <p>For 50 years, WWF has been protecting the future of nature. The world's leading conservation organization, WWF works in 100 countries and is supported by 1.2 million members in the United States and close to 5 million globally.</p>
        <footer>From WWF's Website</footer>
    </blockquote>
    <br/>




    <h1>Description Lists</h1>
    <p> The dl element indicates a description list:</p>
    <dl>
        <dt> Coffee</dt>
        <dd>-- black hot drink</dd>
        <dt>Milk</dt>
        <dd>--White  cold drink</dd>
    </dl>
    <p>Use the .dl-horizontal class line up the description list side-by-side when the browser window expands:</p>

    <dl class="dl-horizontal">
        <dt> Coffee</dt>
        <dd>-- black hot drink</dd>
        <dt>Milk</dt>
        <dd>--White  cold drink</dd>
    </dl>
    <br/>


    <h1>Code Snippets and Keyboard Inputs</h1>
    <p>Inline snippets of code should be embedded in the code element:</p>
    <p>The following HTML elements: <code>span</code>, <code>section</code>, and <code>div</code> defines a section in a document.</p>
    <p>Use <kbd>ctrl + p</kbd> to open the print dialog box</p>
    <br/>


    <pre>
         Text in a pre element
         is displayed in a fixed-width
         font, and it preserves
         both      spaces and
         line breaks.
    </pre>
    <br/>


    <h2>Contextule Colors</h2>
    <p> Use the contextule classes to provide "meaning through colors":</p>
    <p class="text-muted"> This is text is muted.</p>
    <p class="text-primary"> This is text is primary.</p>
    <p class="text-success"> This is text is success.</p>
    <p class="text-info"> This is text is info.</p>
    <p class="text-warning"> This is text is warning.</p>
    <p class="text-danger"> This is text is danger.</p>


    <h2>Contextule Backgrounds</h2>
    <p> Use the contextule classes to provide "meaning through colors":</p>
    <p class="bg-muted"> This is Backgrounds is muted.</p>
    <p class="bg-primary"> This is Backgroundsis primary.</p>
    <p class="bg-success"> This is Backgrounds is success.</p>
    <p class="bg-info"> This is Backgroundsis info.</p>
    <p class="bg-warning"> This is Backgrounds is warning.</p>
    <p class="bg-danger"> This is Backgrounds is danger.</p>
    <br/>

    <h2>Typography</h2>
    <p class="text-lowercase">Lowercased text.</p>
    <p class="text-uppercase">Uppercased text.</p>
    <p class="text-capitalize">Capitalized text.</p>
    <br/>


    <p>The class .list-unstyled removes the default list-style and left margin on list items (immediate children only):</p>
    <ul class="list-unstyled">
        <li>Coffee</li>
        <li>Tea
            <ul>
                <li>Black tea</li>
                <li>Green tea</li>
            </ul>
        </li>
        <li>Milk</li>
    </ul>

    <p>list-styled</p>
    <ul class="list-styled">
        <li>Coffee</li>
        <li>Tea
            <ul>
                <li>Black tea</li>
                <li>Green tea</li>
            </ul>
        </li>
        <li>Milk</li>
    </ul>
    <p>list-inline</p>
    <ul class="list-inline">
        <li>Coffee</li>
        <li>Tea </li>
        <li>Water </li>
        <li>Juices</li>
    </ul>

</div>

</body>
</html>